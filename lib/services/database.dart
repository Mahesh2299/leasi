import 'package:cloud_firestore/cloud_firestore.dart';

class DatabaseService{
  final String uid;
  DatabaseService({ this.uid });
  
  final CollectionReference brewCollection= Firestore.instance.collection('brews');

  Future updateUserData(String name, int level, int rank) async {
    return await brewCollection.document(uid).setData({
      'name': name,
      'level': level,
      'rank': rank,
    });
  }
}