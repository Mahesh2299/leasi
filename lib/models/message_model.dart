import 'package:leasi/models/user_model.dart';

class Message {
  final User sender;
  final String time;
  final String text;
  final bool isLiked;
  final bool unread;

  Message({this.sender, this.time, this.text, this.isLiked, this.unread});

}

final User currentUser = User(id: 0, name: 'Sena', imageUrl: 'assets/images/sena.jpg');
final User yui = User(id: 1, name: 'Yui', imageUrl: 'assets/images/yui.jpg');
final User ryuu = User(id: 2, name: 'Ryuu', imageUrl: 'assets/images/ryuu.jpg');
final User suya = User(id: 3, name: 'Suya', imageUrl: 'assets/images/suya.jpg');
final User haruka = User(id: 4, name: 'Haruka', imageUrl: 'assets/images/haruka.jpg');
final User neji = User(id: 4, name: 'Neji', imageUrl: 'assets/images/neji.jpg');
final User hanabi = User(id: 4, name: 'Hanabi', imageUrl: 'assets/images/hanabi.jpg');
final User iruka = User(id: 4, name: 'Iruka', imageUrl: 'assets/images/iruka.jpg');

//Favorite Contacts
List<User> favorites = [yui, haruka, suya, hanabi, neji, iruka];

// Example chats on home screen
List<Message> chats = [
];

// Example chats in chat screen
List<Message> messages = [
  Message(
    sender: currentUser,
    time: '5:36 PM',
    text: "What is Database??",
    isLiked: true,
    unread: true
  ),
  Message(
    sender: haruka,
    time: '5:36 PM',
    text: "How can I help you?",
    isLiked: false,
    unread: true
  ),
  Message(
    sender: ryuu,
    time: '7:30 PM',
    text: "Hi, I'm LeAsi...')",
    isLiked: false,
    unread: true
  )
];