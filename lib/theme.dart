import 'package:flutter/material.dart';

TextStyle listTitleDefaultTextStyle= TextStyle(
  color: Colors.white70, 
  fontSize: 20.0, 
  fontWeight: FontWeight.w300,
);
TextStyle listTitleSelectedTextStyle= TextStyle(
  color: Colors.white, 
  fontSize: 20.0, 
  fontWeight: FontWeight.w300,
);

Color selectedColor= Color(0xff4ac8ea);
Color drawerBackgroundColor= Color(0xFF42A5F5);
