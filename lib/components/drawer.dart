import 'package:flutter/material.dart';
import 'package:leasi/screens/home/home.dart';
import 'package:leasi/services/auth.dart';
import 'package:leasi/screens/dialog_flow.dart';
import 'package:leasi/screens/pages/quizz/quiz.dart';
import 'package:leasi/screens/pages/library.dart';

class NavigationDrawer extends StatefulWidget {
  @override
  _NavigationDrawerState createState() => _NavigationDrawerState();
}

class _NavigationDrawerState extends State<NavigationDrawer> {

  final AuthService _auth = AuthService();
  bool isSelected= false;

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 10.0,
      child: Container(
        width: 300.0,
        color: Color(0xFF6ebaf7),

        child: ListView(
          children: <Widget>[

            SizedBox(height: 20),
            Center(
              child: CircleAvatar(
                backgroundColor: Colors.white,
                backgroundImage: AssetImage('assets/images/logo.png'),
                radius: 50.0,
              ),
            ),
            SizedBox(height: 15),
            Center(
              child: Text(
                'Test',
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Center(
              child: Text(
                'test@test.com',
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                  fontWeight: FontWeight.w100,
                ),
              ),
            ),
            Divider(color: Colors.grey, height: 20.0),
           
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 2),
              child: Card(
                child: Ink(
                  color: Colors.blue[500],
                  child: ListTile(                  
                    leading: Icon(Icons.insert_chart, color: Colors.white70,  size: 28),
                    title: Text('Home', style: TextStyle(color: Colors.white70, fontSize: 18.0),),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context){
                          return Home();
                        },
                      ),
                      );
                    },
                  ),
                ),
              ),
            ),

            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 2),
              child: Card(
                child: Ink(
                  color: Colors.blue[500],
                  child: ListTile(                  
                    leading: Icon(Icons.mic, color: Colors.white70,  size: 28),
                    title: Text('Conversation', style: TextStyle(color: Colors.white70, fontSize: 18.0)),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context){
                          return FlutterFactsDialogFlow();
                        }
                      ));
                    },
                  ),
                ),
              ),
            ),



            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 2),
              child: Card(
                child: Ink(
                  color: Colors.blue[500],
                  child: ListTile(                  
                    leading: Icon(Icons.school, color: Colors.white70,  size: 28),
                    title: Text('Tryout', style: TextStyle(color: Colors.white70, fontSize: 18.0)),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context){
                          return Quizzler();
                        }
                      ));
                    },
                  ),
                ),
              ),
            ),

            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 2),
              child: Card(
                child: Ink(
                  color: Colors.blue[500],
                  child: ListTile(                  
                    leading: Icon(Icons.offline_pin, color: Colors.white70,  size: 28),
                    title: Text('Library', style: TextStyle(color: Colors.white70, fontSize: 18.0)),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context){
                          return Library();
                        }
                      ));
                    }
                  ),
                ),
              ),
            ),

            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 2),
              child: Card(
                child: Ink(
                  color: Colors.blue[500],
                  child: ListTile(                  
                    leading: Icon(Icons.power_settings_new, color: Colors.white70, size: 28),
                    title: Text('Logout', style: TextStyle(color: Colors.white70, fontSize: 18.0)),
                     onTap: ()async {
                        await _auth.signOut();
                      },
                  ),
                ),
              ),
            ),
           
          ],
        ),
      ),
    );
  }
}