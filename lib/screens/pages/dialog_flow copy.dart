// import 'dart:math';

// import 'package:flutter/material.dart';
// import 'package:leasi/widgets/fact_message.dart';
// import 'package:flutter_dialogflow/dialogflow_v2.dart';
// import 'package:flutter_tts/flutter_tts.dart';
// import 'package:speech_to_text/speech_recognition_error.dart';
// import 'package:speech_to_text/speech_recognition_result.dart';
// import 'package:speech_to_text/speech_to_text.dart';


// class FlutterFactsDialogFlow extends StatefulWidget {
//   FlutterFactsDialogFlow({Key key, this.title}) : super(key: key);

//   final String title;

//   @override

//   _FlutterFactsDialogFlowState createState() => new _FlutterFactsDialogFlowState();
// }

// class _FlutterFactsDialogFlowState extends State<FlutterFactsDialogFlow> {

//   final List<FactsMessage> _messages = <FactsMessage>[];
//   final TextEditingController _textController = new TextEditingController();

//   String outputText= '';
//   bool _hasSpeech= false;
//   String _currentLocaleId= 'en_US';
//   double minSoundLevel= 50000;
//   double maxSoundLevel= -50000;
//   double level= 0.0;

//   final SpeechToText speech= SpeechToText();

//   @override
//   void initState(){
//     super.initState();
//     initSpeechState();
//   }


//   Widget _queryInputWidget(BuildContext context) {

//     return Container(
//       child: Container(
//         margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
//         child: Row(
//           children: <Widget>[
//             Flexible(
//               child: TextField(
//                 controller: _textController,
//                 onSubmitted: _submitQuery,
//                 decoration: InputDecoration.collapsed(hintText: "Send a message"),
//               ),
//             ),
//             Container(
//               margin: EdgeInsets.symmetric(horizontal: 4.0),
//               child: IconButton(
//                   icon: Icon(
//                     Icons.send,
//                     color: Colors.blue,
//                     size: 30,
//                   ),
//                   onPressed: () => _submitQuery(_textController.text),
//                   ),
//             ),
            
//             Container(
//               decoration: BoxDecoration(
//                 boxShadow: [
//                   BoxShadow(
//                   blurRadius: .26,
//                   spreadRadius: level*1.25,
//                   color: Colors.black12),
//                 ],
//                 color: Colors.white,
//                 borderRadius: BorderRadius.all(Radius.circular(50)),
//               ),
//               child: FloatingActionButton(
//                 onPressed: (){
//                   !_hasSpeech ||speech.isListening ? null : startListening();
//                 } ,
//                 child: Icon(
//                   Icons.mic,
//                   color: speech.isListening ? Colors.redAccent : Colors.white,
//                   ),
//               ),
//             ), 



//           ],
//         ),


        

//       ),
      
//     );
//   }
  
//   void _dialogFlowResponse(query) async {
//     _textController.clear();
//     AuthGoogle authGoogle =
//     await AuthGoogle(fileJson: "assets/leasi-wgqgws-2134ec61afc7.json").build();
//     Dialogflow dialogFlow =
//     Dialogflow(authGoogle: authGoogle, language: Language.english);
//     AIResponse response = await dialogFlow.detectIntent(query);
//     FactsMessage message = FactsMessage(
//       text: response.getMessage() ??
//            CardDialogflow(response.getListMessage()[0]).title,
//       name: "LeAsi",
//       type: false,
//     );

//     final FlutterTts flutterTts= FlutterTts();

//     speak()async{
//       await flutterTts.setLanguage("en-US");
//       await flutterTts.setPitch(1);
//       await flutterTts.setSpeechRate(1);
//       await flutterTts.speak(response.getMessage() ??
//            CardDialogflow(response.getListMessage()[0]).title);
//     }
      
//     setState(() {
//       _messages.insert(0, message);

//       speak();

//     });
//   }

//   void _submitQuery(String text) {
//     _textController.clear();
//     FactsMessage message = new FactsMessage(
//       text: text,
//       name: "Test",
//       type: true,
//     );

//     setState(() {
//       _messages.insert(0, message);
//     });
//     _dialogFlowResponse(text);
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         centerTitle: true,
//         title: Text("LeAsi Conversation"),
//       ),
//       body: Column(children: <Widget>[
//         Flexible(
//             child: ListView.builder(
//               padding: EdgeInsets.all(8.0),
//               reverse: true, //To keep the latest messages at the bottom
//               itemBuilder: (_, int index) => _messages[index],
//               itemCount: _messages.length,
//             )),
//         Divider(height: 1.0),
//         Container(
//           decoration: new BoxDecoration(color: Colors.white),
//           child: _queryInputWidget(context),
//         ),
//       ]),
//     );
//   }


  
//   Future<void> initSpeechState() async {
//     bool hasSpeech= await speech.initialize(
//       onError: errorListener, onStatus: statusListener
//     );
//     if(!mounted) return;
//     setState(() {
//       _hasSpeech= hasSpeech;
//     });
//   }

//   void statusListener(String status) {
//     print(status);
//   }
      
//   void errorListener(SpeechRecognitionError errorNotification) {
//     print(errorNotification);
//   }

//   startListening() {
//     speech.listen(
//       onResult: resultListener,
//       listenFor: Duration(seconds: 10),
//       localeId: _currentLocaleId,
//       onSoundLevelChange: soundLevelListener,
//       cancelOnError: true,
//       onDevice: true,
//       listenMode: ListenMode.confirmation
//     );
//   }

//   void resultListener(SpeechRecognitionResult result) {
//     if(result.finalResult)
//       setState(() {
//         _submitQuery(result.recognizedWords);
//       });

//   }

//   soundLevelListener(double level) {
//     minSoundLevel= min(minSoundLevel, level);
//     maxSoundLevel= max(maxSoundLevel, level);
//     setState(() {
//       this.level=level;
//     });
//   }
// }
