import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class Library extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Materials",style: TextStyle(fontSize: 25.0,fontWeight: FontWeight.bold),),
          centerTitle: true,
          backgroundColor: Colors.blue,
    ),
      body: Center(
        child: MaterialButton(
            splashColor: Colors.blue,
            onPressed: (){
              Navigator.of(context)
                  .push(
                MaterialPageRoute(
                  builder: (context) => Download()
                )
              );
            },
            color: Colors.blue,
            textColor: Colors.white,
            elevation: 10.0,
            hoverElevation: 30.0,
            highlightElevation: 20.0,
            child: Text("Principles Of Database Design",
            style: TextStyle(
            fontSize: 20.0,
                fontWeight: FontWeight.bold
           ),
          ),
          shape: StadiumBorder(),
          padding: EdgeInsets.all(16.0),
        ),
      ),
    );
  }
}
class Download extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text("Materials",style: TextStyle(fontSize: 25.0,fontWeight: FontWeight.bold),),
          centerTitle: true,
          backgroundColor: Colors.blue,
    ),
    body: Center(
      child: Container(
        height: 400,
        alignment: Alignment.center,
        color: Colors.white12,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                MaterialButton(splashColor: Colors.blue,
                  onPressed: (){},
                  color: Colors.blue,
                  textColor: Colors.white,
                  elevation: 10.0,
                  hoverElevation: 30.0,
                  highlightElevation: 20.0,
                  child: Text("Module 1",
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                  shape: StadiumBorder(),
                  padding: EdgeInsets.all(16.0),
                ),
                IconButton(icon: Icon(Icons.file_download),
                  onPressed: (){},
                  iconSize: 40.0,
                  color: Colors.blue,
                  highlightColor: Colors.lightBlueAccent,),
              ],
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                MaterialButton(splashColor: Colors.blue,
                  onPressed: (){},
                  color: Colors.blue,
                  textColor: Colors.white,
                  elevation: 10.0,
                  hoverElevation: 30.0,
                  highlightElevation: 20.0,
                  child: Text("Module 2",
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                  shape: StadiumBorder(),
                  padding: EdgeInsets.all(16.0),
                ),
                IconButton(icon: Icon(Icons.file_download),
                  onPressed: (){},
                  iconSize: 40.0,
                  color: Colors.blue,
                  highlightColor: Colors.lightBlueAccent,),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                MaterialButton(splashColor: Colors.blue,
                  onPressed: (){},
                  color: Colors.blue,
                  textColor: Colors.white,
                  elevation: 10.0,
                  hoverElevation: 30.0,
                  highlightElevation: 20.0,
                  child: Text("Module 3",
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                  shape: StadiumBorder(),
                  padding: EdgeInsets.all(16.0),
                ),
                IconButton(icon: Icon(Icons.file_download),
                  onPressed: (){},
                  iconSize: 40.0,
                  color: Colors.blue,
                  highlightColor: Colors.lightBlueAccent,),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                MaterialButton(splashColor: Colors.blue,
                  onPressed: (){},
                  color: Colors.blue,
                  textColor: Colors.white,
                  elevation: 10.0,
                  hoverElevation: 30.0,
                  highlightElevation: 20.0,
                  child: Text("Module 4",
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                  shape: StadiumBorder(),
                  padding: EdgeInsets.all(16.0),
                ),
                IconButton(icon: Icon(Icons.file_download),
                  onPressed: (){},
                  iconSize: 40.0,
                  color: Colors.blue,
                  highlightColor: Colors.lightBlueAccent,),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                MaterialButton(splashColor: Colors.blue,
                  onPressed: (){},
                  color: Colors.blue,
                  textColor: Colors.white,
                  elevation: 10.0,
                  hoverElevation: 30.0,
                  highlightElevation: 20.0,
                  child: Text("Module 5",
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                  shape: StadiumBorder(),
                  padding: EdgeInsets.all(16.0),
                ),
                IconButton(icon: Icon(Icons.file_download),
                  onPressed: (){},
                  iconSize: 40.0,
                  color: Colors.blue,
                  highlightColor: Colors.lightBlueAccent,),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                MaterialButton(splashColor: Colors.blue,
                  onPressed: (){},
                  color: Colors.blue,
                  textColor: Colors.white,
                  elevation: 10.0,
                  hoverElevation: 30.0,
                  highlightElevation: 20.0,
                  child: Text("Module 6",
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                  shape: StadiumBorder(),
                  padding: EdgeInsets.all(16.0),
                ),
                IconButton(icon: Icon(Icons.file_download),
                  onPressed: (){},
                  iconSize: 40.0,
                  color: Colors.blue,
                  highlightColor: Colors.lightBlueAccent,),
              ],
            ),
          ]
        ),
      )
    )
    );


  }
}
