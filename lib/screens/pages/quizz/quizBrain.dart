import 'question.dart';

class QuizBrain{ 
  int _questionNumber = 0;

  List<Question> _questionBank = [
    Question('A database has data and relationships.', true),
    Question('In an enterprise-class database system, business users interact directly with database applications, which directly access the database data.', false),
    Question('Applications are programs that interact directly with the database.', false),
    Question('The purpose of a database is to help people stop using spreadsheets.', false),
    Question('Structured Query Language (SQL) is an internationally recognized standard language that is understood by all commercial database management system products.', true),
    Question('A database has a built-in capability to create, process and administer itself.', false),
    Question('Enterprise Resource Planning (ERP) is an example of a single user database.', false),
    Question('A database design may be based on existing data.',true),
    Question('Database applications are seldom intended for use by a single user.', false),
    Question('A primary goal of a database system is to share data with multiple users.', true),
    Question('If a table is in 1NF and does not have a composite key, then it is in 2NF',true),
    Question('The primary key does not necessarily have to be unique for a given table', false),
    Question('If you use a GROUP BY command, you cannot see the non- aggregated data in the same query.', true),
    Question('A View is a saved query.', true),
    Question('A Database cursor defines a select statement and allows you to loop through the set of data and execute instructions on each row', true),
  ];

  void nextQuestion() {
    if (_questionNumber < _questionBank.length - 1) {
      _questionNumber++;
    }
  }

   String getQuestionText() {
    return _questionBank[_questionNumber].questionText;
  }

   bool getCorrectAnswer() {
    return _questionBank[_questionNumber].questionAnswer;
  }
  bool isFinished() {
    if (_questionNumber >= _questionBank.length - 1) {
      print('Now returning true');
      return true;
    } 
    else {
      return false;
    }
  }
  void reset() {
    _questionNumber = 0;
  }
}