import 'package:flutter/material.dart';
import 'package:leasi/models/user.dart';
import 'package:leasi/screens/authenticate/authenticate.dart';
import 'package:leasi/screens/home/home.dart';
import 'package:provider/provider.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    if(user==null){
      return Authenticate();
    }else {
      return Home();
    }
  }
}