import 'package:flutter/material.dart';
import 'package:leasi/screens/authenticate/sign_in.dart';
import 'package:leasi/services/auth.dart';
import 'package:leasi/shared/loading.dart';

class Register extends StatefulWidget {

  final Function toggleView;
  Register({this.toggleView});

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {

  final AuthService _auth = AuthService();
  final _formkey = GlobalKey<FormState>();
  bool loading= false;

  String email = '';
  String password = '';
  String error = '';

  @override
    Widget build(BuildContext context) {
    return loading ? Loading(): Scaffold(
        body: Stack(
          children: <Widget>[
            Container(
              color: Color(0xFF42A5F5),
              child: Image(
                image: AssetImage('assets/images/signup.png'),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 300),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.white,
              ),
              child: Padding(
                padding: EdgeInsets.all(23),
                child: ListView(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.fromLTRB(0,0,0,20),
                      child: Container(
                        color: Color(0xffffffff),
                        child: Form(
                          key: _formkey,
                          child: Column(
                            children:<Widget>[
                              TextFormField(
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'User Name',
                                  prefixIcon: Icon(Icons.person_outline),
                                  labelStyle: TextStyle(
                                    fontSize: 15,
                                  ),
                                ),
                                validator: (val)=>val.isEmpty ? 'Email field cannot be empty!' : null,
                                onChanged: (val) {
                                  setState(() =>email = val                   
                                  );
                                }
                              ),

                              SizedBox(height: 20.0),
                              TextFormField(
                                  decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Password',
                                  prefixIcon: Icon(Icons.lock_outline),
                                  labelStyle: TextStyle(
                                    fontSize: 15,
                                  ),
                                ),
                                obscureText: true,
                                validator: (val)=>val.length < 6 ? 'Password must be at least 6 characters long!' : null,
                                onChanged: (val) {
                                  setState(() =>password = val                   
                                  );

                                },

                              ),

                              // SizedBox(height: 20.0),
                              Padding(
                                padding: const EdgeInsets.only(top: 30),
                                child: MaterialButton(
                                  onPressed: () async {
                                    if(_formkey.currentState.validate()){
                                      setState(() {
                                        loading= true;
                                      });
                                      dynamic result= await _auth.registerWithEmailAndPassword(email,password);
                                      if(result==null){
                                        setState((){ 
                                          loading= false;
                                          error='Please supply a  valid email';
                                        });
                                      }
                                    }
                                  },
                                  child: Text(
                                    'SIGN UP',
                                    style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  color: Color(0xFF42A5F5),
                                  elevation: 0,
                                  minWidth: 400,
                                  height: 50,
                                  textColor: Colors.white,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)
                                  ),
                                ),
                              ),
                              

                              Padding(
                                padding: EdgeInsets.only(bottom: 10, top: 25),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text("Already have an account ",style: TextStyle(fontWeight: FontWeight.bold),),
                                    GestureDetector(
                                      onTap: (){
                                        Navigator.push(context, MaterialPageRoute(builder: (context){
                                          return SignIn();
                                        }));
                                      },
                                      child: Text("Sign In",style: TextStyle(fontWeight: FontWeight.bold,color: Color(0xFF42A5F5)),),
                                    )
                                  ],
                                ),
                              ),
                              

                              Text(
                                error,
                                style: TextStyle(color: Colors.red, fontSize: 14.0),
                              ),
                            ]
                          ),
                        ),
                      ),
                      )
                  ],

                )
              ),
            )
          ],
        ),
    );
  }
}